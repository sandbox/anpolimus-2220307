Responsive slider
=====================
The Responsive slider module integrates Responsive slider library (http://w3widgets.com/responsive-slider).
Module contains views style plugin and field formatter for displaying slider in different ways.

Also, developers could use responsive slider-in-code by using defined theme style.
You just need to define "items" and "show_rounds" variables in render array.
'responsive_slider' => array(
   'variables' => array(
     'items' => NULL,
      'show_rounds' => NULL,
   ),
   'template' => 'responsive_slider-list',
),

This module requires jquery update module and jquery 1.8+ library.

Responsive slider module requires last version of responsive_slider js lib.
But, if it is absent in libraries, module uses last stable version in it.

