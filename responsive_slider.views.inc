<?php

/**
 *  Implements hook_views_plugins().
 */
function responsive_slider_views_plugins() {
  return array(
    'module' => 'responsive_slider',
    'style' => array(
      'responsive_slider' => array(
        'title' => t('Responsive slider'),
        'handler' => 'responsive_slider_plugin_style_slider',
        'path' => drupal_get_path('module', 'responsive_slider'),
        'help' => t('Display list as responsive slider'),
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
        'even empty' => TRUE,
      ),
    ),
  );
}