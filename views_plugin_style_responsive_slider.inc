<?php

class responsive_slider_plugin_style_slider extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['style_name'] = array('default' => 'large');
    $options['image_field'] = array('default' => '');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $image_styles = image_styles();
    $image_style_options = array();
    foreach ($image_styles as $key=>$data) {
      $image_style_options[$key] = $data['label'];
    }

    $form['style_name'] = array(
      '#type' => 'select',
      '#title' => t('Select style name'),
      '#description' => t('Select style name of slider images'),
      '#default_value' => $this->options['style_name'],
      '#options'        => $image_style_options,
    );

    $field_options = array('' => t('< none >'));
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_options[$id] = $handler->ui_name(FALSE);
    }

    $form['image_field'] = array(
      '#type' => 'select',
      '#title' => t('Select image field'),
      '#description' => t('Select image field'),
      '#default_value' => $this->options['image_field'],
      '#options'        => $field_options,
    );
  }

  function render() {
    _responsive_slider_add_js();
    $render_data = array();
    $image_field_name = 'field_' . $this->options['image_field'];
    $image_style_name = $this->options['style_name'];
    foreach ($this->view->result as $item) {
      $render_data[] = array(
        'img' => theme('image_style', array(
          'style_name' => $image_style_name,
          'path' => $item->{$image_field_name}[0]['raw']['uri'],
        )),
        'caption' => 'test',
      );
    }
    return array(
      '#theme' => 'responsive_slider',
      '#items' => $render_data,
      '#show_rounds' => TRUE,
    );
  }
}