<div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true" data-interval="5000" data-transitiontime="300">
  <div class="slides" data-group="slides">
    <ul>
      <?php foreach ($items as $item): ?>
        <li>
          <div class="slide-body" data-group="slide">
            <?php print render($item['img']); ?>
            <?php if (!empty($item['caption'])): ?>
              <div class="caption-box">
                <div class="caption-box-in">
                  <div class="pic-foto"></div>
                  <div class="pinterest"></div>
                  <?php foreach ($item['caption'] as $caption_code=>$caption_item): ?>
                    <div class="caption-<?=$caption_code;?>" data-animate="slideAppearLeftToRight" data-delay="800" data-length="300">
                      <?php print $caption_item;?>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            <?php endif; ?>
          </div>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
  <div class="slider-control left"><a href="#" data-jump="prev"><i class="glyphicon glyphicon-chevron-left"></i></a></div>
  <div class="slider-control right"><a href="#" data-jump="next"><i class="glyphicon glyphicon-chevron-right"></i></a></div>
  <?php if ($show_rounds): ?>
    <div class="pages">
      <?php for ($i = 1; $i <= count($items); $i++): ?>
        <a class="page" href="#" data-jump-to="<?php print $i;?>"><?php print $i; ?></a>
      <?php endfor; ?>
    </div>
  <?php endif; ?>
</div>
